import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Collapse from '@material-ui/core/Collapse';
// import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import axios from 'axios';
import InputBase from '@material-ui/core/InputBase';
import checkBtn from './img/icons8-double-tick-30.png';
import fbBtn from './img/icons8-facebook-480.png';
import twitterBtn from './img/icons8-twitter-480.png';
import instaBtn from './img/icons8-instagram-480.png';

// import { parentPort } from 'worker_threads';


import SortIcon from './img/sortAse.png';

const useStyles = makeStyles(theme => ({
    textField: {
        width: 100,
        height: 30,
    },
}));
const defaultProps = {
    bgcolor: 'background.paper',
    border: 1,
    borderColor: '#6495ED',
    borderRadius: '5px',
    style: { width: '260px', height: '50px', padding: '10px' },
};


export const HeaderWithIcon = props => {
    return (
        <Box display="flex">
            <p style={{ margin: '0px auto', fontWeight: 'bold' }}>{props.setHeadName}</p>
            <img src={SortIcon} style={{ width: '17px', height: '17px', marginTop: '2px', marginRight: '20px' }} />
        </Box>
    )
}
export const GearsChip = props => {
    return (
        <div>
            <Chip label="Nikon D5300" variant="outlined" style={{ marginRight: '10px' }} />
            <Chip label="canon 5D" variant="outlined" style={{ marginRight: '10px' }} />
            <Chip label="Nikon D5300" variant="outlined" />
            <br />
            <br />
            <Chip label="canon 5D" variant="outlined" style={{ marginRight: '10px' }} />
            <Chip label="Nikon D5300" variant="outlined" style={{ marginRight: '10px' }} />
            <Chip label="canon 5D" variant="outlined" />
        </div>
    )
}
// mobile Proccess Start
export class CollapseForMob extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMsg: '',
            successData: '',
        };

        this.handleSubmitForMob = this.handleSubmitForMob.bind(this);

    }

    handleSubmitForMob = event => {
        event.preventDefault();
        let err = '';
        var inputMobNo = this.props.setUserMobNo;
        var user_Id = this.props.setUserId;
    
        axios.post("http://13.234.236.42:3771/admin/sendOTP?mob=+91" + inputMobNo + "&user_id=" + user_Id)
        .then(res => {
            this.setState({ successData: res.data.message });
        })
    }
    render(){
        if(this.state.successData === 'OTP sent successfully')
        {
            return(
                <Fragment>
                    <OTPVerifyForMob setUserId={this.props.setUserId} setUserMobNo={this.props.setUserMobNo}/>
                    {/* {setTimeout(function(){<ResendMobOTP />}, 8000)} */}
                </Fragment>
            )
        }
    return (
        <form onSubmit={this.handleSubmitForMob}>
            <Box display="flex" style={{ textAlign: 'center' }}>
                <div style={{ border: "1px solid #6495ED", width: '200px', borderRadius: '5px' }}>
                    <p>{this.props.setUserMobNo}</p>
                </div>
                <Button
                    variant="contained"
                    type="submit"
                    style={{ height: '30px', padding: '0px 10px', fontSize: '13px', textTransform: 'none', margin: '8px 20px', backgroundColor: '#6495ED', color: '#fff' }}>
                    verify
                </Button>
            </Box>
        </form>
    );
    }
}
// Otp verify Form
export class OTPVerifyForMob extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobNoOTP: '',
            errorMsg: '',
            successData: '',
        };

        this.changeHandlerForOTP = this.changeHandlerForOTP.bind(this);
        this.handleSubmitForOTP = this.handleSubmitForOTP.bind(this);

    }
    // change handle for OTP
    changeHandlerForOTP = event => {
        let nam = event.target.name;
        let val = event.target.value;
        let err = '';
    
        if (val.length < 4) {
            err = 'length must be 10 digits';
        }
        this.setState({
            errorMsg: err
        });
        this.setState({
            [nam]: val
        });
    }

    handleSubmitForOTP = event => {
        event.preventDefault();
        let err = '';
        var inputOTP = this.state.mobNoOTP;
        var user_Id = this.props.setUserId;
        var mob_no = this.props.setUserMobNo;
        
        if ((inputOTP.length < 4 || inputOTP.length > 4) ) {
            err = 'length must be 6 digits';
        }
        else
        {
            axios.post("http://13.234.236.42:3771/admin/verifyOTP?mob=+91" + mob_no + "&user_id=" + user_Id+ "&otp=" + inputOTP)
            .then(res => {
                this.setState({ successData: res.data.mesaage });
                console.log(res.data.mesaage )
            })
            console.log("under2",inputOTP,user_Id,mob_no)
        }
       
    }
    render(){
        if(this.state.successData === '')
        {

        }
    return (
        <form onSubmit={this.handleSubmitForOTP}>
            <p style={{ fontSize: '10px', color: 'red', margin: '0px' }}>{this.state.errorMsg}</p>
            <Box display="flex" borderColor="primary.main"{...defaultProps}>
                <br />
                <InputBase
                    placeholder="enter otp"
                    inputProps={{ 'aria-label': 'search' }}
                    name="mobNoOTP"
                    onChange={this.changeHandlerForOTP}
                    style={{ width: '100%' }}
                    required
                />
                <Button
                    variant="contained"
                    type="submit"
                    style={{ height: '30px', padding: '0px 10px', fontSize: '13px', textTransform: 'none', marginLeft: '10px', backgroundColor: '#35F04C', color: '#fff' }}>
                    OTP Verify
                    </Button>
            </Box>
        </form>
    );
    }
}
// resend Otp
export class ResendMobOTP extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobNoOTP: '',
            errorMsg: '',
            successData: '',
        };

        this.changeHandlerForOTP = this.changeHandlerForOTP.bind(this);
        this.handleSubmitForOTP = this.handleSubmitForOTP.bind(this);

    }
    // change handle for OTP
    changeHandlerForOTP = event => {
        let nam = event.target.name;
        let val = event.target.value;
        let err = '';
    
        if (val.length < 4) {
            err = 'length must be 10 digits';
        }
        this.setState({
            errorMsg: err
        });
        this.setState({
            [nam]: val
        });
    }

    handleSubmitForOTP = event => {
        event.preventDefault();
        let err = '';
        var inputOTP = this.state.mobNoOTP;
        var user_Id = this.props.setUserId;
        
        if ((inputOTP.length < 4 || inputOTP.length > 4) ) {
            err = 'length must be 6 digits';
        }
        else
        {
            // axios.post("http://13.234.236.42:3771/admin/savekyc?pan_no=" + inputAcoNo + "&photographer_id=" + photogra_Id)
            // .then(res => {
            //     this.setState({ successData: res.data.mesaage });
            //     console.log(inputAcoNo)
            // })
            console.log("under2",inputOTP,user_Id)
        }
       
    }
    render(){
    return (
        <form onSubmit={this.handleSubmitResendOTP}>
            <p style={{ fontSize: '10px', color: 'red', margin: '0px' }}>{this.state.errorMsg}</p>
            <Box display="flex" borderColor="primary.main"{...defaultProps}>
                <br />
                <Button
                    variant="contained"
                    type="submit"
                    style={{ height: '30px', padding: '0px 10px', fontSize: '13px', textTransform: 'none', marginLeft: '10px', backgroundColor: '#35F04C', color: '#fff' }}>
                    Resend
                    </Button>
            </Box>
        </form>
    );
    }
}
// Final Div After Verification(mob)
export class AfterMobileNoVer extends React.Component {
    render() {
        return (
            <Box display="flex">
                <p>{this.props.setUserMobNo}</p>
                <img src={checkBtn} style={{ width: '30px', height: '30px', }} className="setDistwoItem" />
            </Box>
        )
    }
}
// mobile Proccess End
export const CollapseForEmail = props => {
    // const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <div>
            <div style={{ display: 'flex' }}>
                <p style={{ minWidth: '230px' }}>{props.emailSet}</p>
                <div className="setDistwoItem">
                    <Button
                        variant="contained"
                        color="primary"
                        style={{ height: '35px', padding: '0px 20px' }}
                        onClick={handleExpandClick}
                        aria-expanded={expanded}
                        aria-label="show more">
                        Verify</Button>
                </div>
            </div>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <h5 style={{ color: 'red' }}>Please check your email and click on the verification link.</h5>
            </Collapse>
        </div>
    );
}
// Start PAN,GST AND Aadhar Proccess
export class BeforeAccAdded extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accNoForPGA: '',
            errorMsg: '',
            successData: '',
        };

        this.changeHandlerForPGA = this.changeHandlerForPGA.bind(this);
        this.handleSubmitForPAN = this.handleSubmitForPAN.bind(this);
        this.savekycURL = "http://13.234.236.42:3771/admin/savekyc?";

    }
    // change handle for PAN
    changeHandlerForPGA = event => {
        let nam = event.target.name;
        let val = event.target.value;
        var setNameForPGA = this.props.setName;
        let err = '';
        if (setNameForPGA === 'pan_no'){
            if (val.length < 10 &&  val.length > 10) {
                err = 'length must be 10 digits';
            }
        }
        if (setNameForPGA === 'gst_no'){
            if(val.length < 15 && val.length > 15)
            {
                err = 'length must be 15 digits';
            }
        }
        if (setNameForPGA === 'adhar_no'){
            if(val.length < 12 && val.length > 12)
            {
                err = 'length must be 12 digits';
            }
        }
        this.setState({
            errorMsg: err
        });
        this.setState({
            [nam]: val
        });
    }

    handleSubmitForPAN = event => {
        event.preventDefault();
        let err = '';
        var inputAcoNo = this.state.accNoForPGA;
        var photogra_Id = this.props.setUserId;
        var setNameForPGA = this.props.setName;
        // console.log()
        
        if (setNameForPGA === 'pan_no') {
            if ((inputAcoNo.length < 10 || inputAcoNo.length > 10) ) {
                err = 'length must be 10 digits';
            }
            else
            {
                axios.post(this.savekycURL + "pan_no=" + inputAcoNo + "&photographer_id=" + photogra_Id)
                .then(res => {
                    this.setState({ successData: res.data.mesaage });
                    console.log(this.savekycURL + "pan_no=" + inputAcoNo + "&photographer_id=" + photogra_Id)
                })
            }
           
        }
        if (setNameForPGA === 'gst_no') {
            if ((inputAcoNo.length < 15 || inputAcoNo.length > 15) ) {
                err = 'length must be 15 digits';
            }
            else
            {
                axios.post(this.savekycURL + "gst_no=" + inputAcoNo + "&photographer_id=" + photogra_Id)
                .then(res => {
                    this.setState({ successData: res.data.mesaage });
                    console.log(res.data)
                })
            }
           
        }
        if (setNameForPGA === 'adhar_no') {
            if ((inputAcoNo.length < 12 || inputAcoNo.length > 12) ) {
                err = 'length must be 12 digits';
            }
            else
            {
                axios.post(this.savekycURL + "adhar_no=" + inputAcoNo + "&photographer_id=" + photogra_Id)
                .then(res => {
                    this.setState({ successData: res.data.mesaage });
                    console.log(res.data)
                })
            }
           
        }
        this.setState({
            errorMsg: err
        });
       
    }

render() {
    if (this.state.successData === 'saved successfuly') {
        console.log("under1",this.props.setNameForVar,this.props.setUserId)
        return (
            <Fragment>
                <p style={{ fontSize: '10px', color: 'green', margin: '0px' }}>Saved Successfully .Now click On Verified</p>
                <AccVerification setAcoNo={this.state.accNoForPGA} setUserId={this.props.setUserId} setNameForVar={this.props.setNameForVar}/>
            </Fragment>
        )
    }
    return (
        <Fragment>
            <form onSubmit={this.handleSubmitForPAN}>
                <p style={{ fontSize: '10px', color: 'red', margin: '0px' }}>{this.state.errorMsg}</p>
                <Box display="flex" borderColor="primary.main"{...defaultProps}>
                    <br />
                    <InputBase
                        placeholder={this.props.setPlaceholder}
                        inputProps={{ 'aria-label': 'search' }}
                        title="search"
                        name="accNoForPGA"
                        onChange={this.changeHandlerForPGA}
                        style={{ width: '100%' }}
                        required
                    />
                    <Button
                        variant="contained"
                        type="submit"
                        style={{ height: '30px', padding: '0px 10px', fontSize: '13px', textTransform: 'none', marginLeft: '10px', backgroundColor: '#35F04C', color: '#fff' }}>
                        save
                        </Button>
                </Box>
            </form>
        </Fragment>
    );
}
}
export class AccVerification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            verifiedPANNo: this.props.setAcoNo,
            VarSuccessData: '',
            photographerId: this.props.setUserId,
            setVarName : this.props.setNameForVar

        };

        this.handleSubmitForPANVer = this.handleSubmitForPANVer.bind(this);
        this.verifykycURL ="http://13.234.236.42:3771/admin/verifykyc?"

    }

    handleSubmitForPANVer = event => {
        event.preventDefault();

        var verifiedPhotoId = this.state.photographerId;
        var setName = this.state.setVarName;
        console.log("under2", setName)
        if(setName === 'pan')
        {
            axios.post(this.verifykycURL +"pan=1&photographer_id=" + verifiedPhotoId)
            .then(res => {
                this.setState({ VarSuccessData: res.data.mesaage });
            })
        }
        if(setName === 'gst')
        {
            axios.post(this.verifykycURL +"gst=1&photographer_id=" + verifiedPhotoId)
            .then(res => {
                this.setState({ VarSuccessData: res.data.mesaage });
            })
        }
        if(setName === 'aadhar')
        {
            axios.post(this.verifykycURL +"adhar=1&photographer_id=" + verifiedPhotoId)
            .then(res => {
                this.setState({ VarSuccessData: res.data.mesaage });
            })
        }
    }
    render() {
        if (this.state.VarSuccessData === 'verified successfuly') {
            return (
                <Fragment>
                    <p style={{ fontSize: '10px', color: 'green', margin: '0px' }}>Verified Successfully</p>
                    <AfterAccInputAndVar setAcoNo={this.props.setAcoNo} />
                </Fragment>
            )
        }
        return (
            <form onSubmit={this.handleSubmitForPANVer}>
                <Box display="flex" style={{ textAlign: 'center' }}>
                    <div style={{ border: "1px solid #6495ED", width: '200px', borderRadius: '5px' }}>
                        <p>{this.props.setAcoNo}</p>
                    </div>
                    <Button
                        variant="contained"
                        type="submit"
                        style={{ height: '30px', padding: '0px 10px', fontSize: '13px', textTransform: 'none', margin: '8px 20px', backgroundColor: '#6495ED', color: '#fff' }}>
                        verify
                </Button>
                </Box>
            </form>
        );
    }
}
export class AfterAccInputAndVar extends React.Component {
    render() {
        return (
            <Box display="flex">
                <div style={{ width: '100px' }}>
                    {/* <img src={row.original.pan_url} alt="not found"/> */}
                </div>
                <Box display="flex">
                    <p>{this.props.setAcoNo}</p>
                    <img src={checkBtn} style={{ width: '30px', height: '30px', }} className="setDistwoItem" />
                </Box>
            </Box>
        );
    }
}
// End PAN,GST AND Aadhar Proccess

export class FbTwitterInstaVerif extends React.Component {
    render() {
        if (this.props.checkCondForSocial === 'facebook') {
            if ((this.props.checkLink !== "")) {
                return <a href={this.props.checkLink} target="_blank"><img src={fbBtn} style={{ width: '34px', heigh: '34px' }} /></a>
            }
            else {
                return (
                    <p></p>
                )
            }
        }
        if (this.props.checkCondForSocial === 'twitter') {
            if ((this.props.checkLink !== "")) {
                return <a href={this.props.checkLink} target="_blank"><img src={twitterBtn} style={{ width: '34px', heigh: '34px', marginLeft: '10px' }} /></a>
            }
            else {
                return (
                    <p></p>
                )
            }
        }
        if (this.props.checkCondForSocial === 'instagram') {
            if ((this.props.checkLink !== "")) {
                return <a href={this.props.checkLink} target="_blank"><img src={instaBtn} style={{ width: '34px', heigh: '34px', marginLeft: '10px' }} /></a>
            }
            else {
                return (
                    <p></p>
                )
            }
        }
    }
}
