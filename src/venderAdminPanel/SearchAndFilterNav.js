import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios';
// import React from 'react';

class FilterBarForVen extends React.Component
{
    constructor(props) {
        super(props);
        this.state = { value: 'select'};
    
        this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleChange(event) {
        this.setState({ value: event.target.value })
        
        if(event.target.value === 'is_vender'){
          axios.get(`http://13.234.236.42:3771/admin/vendorlist?approved=1&vendor=1`)
      
            .then(res => {
              const profileEmail = res.data.photographer_detail1;
              const photographerDetail2 = res.data.photographer_detail2;
              const photographerDetail4 = res.data.photographer_detail4;
              const photographerDetail3 = res.data.photographer_detail3;
              const photographerSocDet = res.data.photographer_social_details;
              const coverPhotoDet = res.data.photographer_coverphoto;
      
              const photographerFinal = [];
              var mobile = {};
              var social = {};
              var pangst = {};
              var profile = {};
              var coverDetails = {};
      
              for (var i = 0; i < photographerDetail3.length; i++) {
                // Mobile and Email Details
                for (var j = 0; j < photographerDetail2.length; j++) {
                  if (photographerDetail2[j].photographer_id === photographerDetail3[i].photographer_id) {
                    mobile = ({
                      "id": photographerDetail2[j].photographer_id,
                      "mobile_no": photographerDetail2[j].mobile_no,
                      "mobile_verified": photographerDetail2[j].is_mobile_verified,
                      "email_verified": photographerDetail2[j].is_email_verified,
                    })
                    break;
                  }
                }
                // PAN,GST,Aadhar Details
                for (var k = 0; k < photographerDetail4.length; k++) {
                  if (photographerDetail4[k].photographer_id === photographerDetail3[i].photographer_id) {
                    pangst = ({
                      "id": photographerDetail4[k].photographer_id,
                      "pan_title": photographerDetail4[k].pan_title,
                      "pan_verified": photographerDetail4[k].pan_verified,
                      "pan_number": photographerDetail4[k].pan_number,
      
                      "adhar_title": photographerDetail4[k].adhar_title,
                      "adhar_number": photographerDetail4[k].adhar_number,
                      "adhar_verified": photographerDetail4[k].adhar_verified,
      
      
                      "gst_title": photographerDetail4[k].gst_title,
                      "gst_verified": photographerDetail4[k].gst_verified,
                    })
                    break;
                  }
                  else {
                    pangst = 0
                  }
                }
                // photographer_social_details
                for (var l = 0; l < photographerSocDet.length; l++) {
                  if (photographerSocDet[l].photographer_id === photographerDetail3[i].photographer_id) {
                    social = ({
                      "id": photographerSocDet[l].photographer_id,
                      "facebook_link": photographerSocDet[l].cf_facebook_link,
                      "Instagram_link": photographerSocDet[l].cf_instagram_link,
                      "Twitter_link": photographerSocDet[l].cf_twitter_link,
                    })
                    break;
                  }
                  else {
                    social = null
                  }
                }
                // profile And Email Details
                for (var m = 0; m < profileEmail.length; m++) {
                  if (profileEmail[m].photographer_id === photographerDetail3[i].photographer_id) {
                    profile = ({
                      "id": profileEmail[m].photographer_id,
                      "profile_image": profileEmail[m].profile_image,
                      "email": profileEmail[m].email,
                      "name": profileEmail[m].full_name
                    })
                    break;
                  }
                  else {
                    profile = 0
                  }
                }
                // Photographer Cover Details
                for (var n = 0; n < coverPhotoDet.length; n++) {
                  if (coverPhotoDet[n].photographer_id === photographerDetail3[i].photographer_id) {
                    coverDetails = ({
                      "id": coverPhotoDet[n].photographer_id,
                      "cover_photo": coverPhotoDet[n].cover_photo,
                    })
                    break;
                  }
                  else {
                    coverDetails = 0
                  }
                }
      
                // final Result Combined
                photographerFinal.push({
                  "photographer_id": photographerDetail3[i].photographer_id,
                  "url": photographerDetail3[i].url,
                  "pangstVar": pangst,
                  "mobile": mobile,
                  "social": social,
                  "profileEmail": profile,
                  "coverDetails": coverDetails,
                })
              }
              this.setState({ tableData: photographerFinal });
              console.log(photographerFinal)
      
            })
          }
      }
      render(){
          return(
            <select value={this.state.value} onChange={this.handleChange.bind(this)}>
            <option value="">select</option>
            <option value="approved">approved</option>
            <option value="is_vender">is_vender</option>
            <option value="vender_contract">vender_contract</option>
            <option value="categoties">categoties</option>
          </select>
          )
      }
}
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
}));

export default function SearchAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
            <FilterBarForVen />
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
