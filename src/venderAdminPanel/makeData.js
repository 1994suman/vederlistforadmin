import axios from 'axios';
import React,{Fragment}from 'react';
import ReactTable from 'react-table-v6';
import Pagination from "./Pagination";
// import checkBtn from './img/icons8-double-tick-30.png';
// import SearchAndFilterNav from './SearchAndFilterNav';
import Button from '@material-ui/core/Button';
import './sass/venderAdminTable.css';

import Box from '@material-ui/core/Box';

import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import verified from './img/download.png';

import { CollapseForMob,AfterMobileNoVer, CollapseForEmail, BeforeAccAdded,AccVerification, AfterAccInputAndVar,FbTwitterInstaVerif ,HeaderWithIcon } from "./allCommonComp"
export class FlavorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: 'all_value', 
                  tableData: [], 
                  searchValue: '',
                  
                };


    this.handleChange = this.handleChange.bind(this);
    this.myChangeHandler = this.myChangeHandler.bind(this);
    this.urlForFilter = "http://13.234.236.42:3771/admin/vendorlist1?";
    this.urlForSearch = "http://13.234.236.42:3771/admin/vendorlist2?";

    this.handleSubmitForSearch = this.handleSubmitForSearch.bind(this);
  }
  // handle Search
  myChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }
  
  handleSubmitForSearch = event => {
    event.preventDefault();

    var searchData = this.state.searchValue;
    axios.get(this.urlForSearch + "value=" + searchData)
      .then(res => {
        this.setState({ tableData: res.data.vendors });
        console.log(this.urlForSearch + "value=" + searchData)
      })

  }
  // handle Filter
  handleChange(event) {
    this.setState({ value: event.target.value })
    if (event.target.value === 'all_value') {
      axios.get(this.urlForFilter + "approved=0&vendor=0&vendor_contract=0")
        .then(res => {
          this.setState({ tableData: res.data.vendors });
          // console.log(res.data)
        })
    }
    if (event.target.value === 'is_vender') {
      axios.get(this.urlForFilter + "approved=0&vendor=1&vendor_contract=0")
        .then(res => {
          this.setState({ tableData: res.data.vendors });
          // console.log(res.data)
        })
    }
    if (event.target.value === 'approved') {
      axios.get(this.urlForFilter + "approved=1&vendor=0&vendor_contract=0")
        .then(res => {
          this.setState({ tableData: res.data.vendors });
          // console.log(res.data)
        })
    }
    if (event.target.value === 'vender_contract') {
      axios.get(this.urlForFilter + "approved=0&vendor=0&vendor_contract=1")
        .then(res => {
          this.setState({ tableData: res.data.vendors });
          // console.log(res.data)
        })
    }
    if (event.target.value === 'select_all') {
      axios.get(this.urlForFilter + "approved=1&vendor=1&vendor_contract=1")
        .then(res => {
          this.setState({ tableData: res.data.vendors });
          // console.log(res.data)
        })
    }
  }

  componentDidMount() {
    axios.get(this.urlForFilter + "approved=0&vendor=0&vendor_contract=0")
      .then(res => {
        this.setState({ tableData: res.data.vendors });
        // console.log(res.data.vendors)
      })
  }

  render() {
    var { tableData } = this.state;
    return (
      <div style={{ marginBottom: '30px' }}>
        <div style={{ display: 'flex' }}>
          <select value={this.state.value} onChange={this.handleChange.bind(this)} style={{ width: '20%', marginLeft: '30px' }}>
            <option value="all_value">select</option>
            <option value="approved">approved</option>
            <option value="is_vender">vender</option>
            <option value="vender_contract">vender_contract</option>
            <option value="select_all">select_all</option>

          </select>
          <form onSubmit={this.handleSubmitForSearch} style={{ width: '80%' }}>
            <div style={{ width: '50%', display: 'flex', justifyContent: 'flex-end', marginLeft: '45%', borderBottom: '1px solid grey' }}>
              <InputBase
                placeholder="Search…"
                inputProps={{ 'aria-label': 'search' }}
                title="search"
                name="searchValue"
                onChange={this.myChangeHandler}
                style={{ width: '100%' }}
              />
              <Button variant="contained" className="rfSubmitBtn" type="submit">
                <SearchIcon style={{ width: '20px', marginTop: '-3px', color: '#fff' }} type="submit" />
              </Button>
            </div>
          </form>
        </div>
        <ReactTable 
          data={tableData}
          columns={[
            {
              Header: (row) => <HeaderWithIcon setHeadName="Photo_id" />,
              accessor: 'photographer_id',
              id: 'photographer_id',
              width: 50,
              Cell: (row) => {
                return <p style={{ fontWeight: 'bold', color: '#6495ED' }}>{row.original.photographer_id}</p>
              },
              filterable: true,
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="Name" />,
              id: 'Name',
              accessor: 'full_name',
              Cell: (row) => {
                return <p style={{ color: 'green' }}>{row.original.full_name}</p>
              },
              minWidth: 250,
              filterMethod: (filter, row) => {
                const id = filter.pivotId || filter.id;
                return (
                  row[id] !== undefined ?
                    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
                );
              }
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="Email" />,
              accessor: 'email',
              id: 'email',
              minWidth: 400,
              filterMethod: (filter, row) => {
                const id = filter.pivotId || filter.id;
                return (
                  row[id] !== undefined ?
                    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
                );
              },
              Cell: (row) => {
                if (row.original.is_email_verified)
                  return (<div style={{ display: 'flex' }}>
                    <p>{row.original.email}</p>
                    <img src={verified} style={{ width: '25px', height: '25px',marginTop:'10px' }} className="setDistwoItem" />
                  </div>)
                else {

                  return (
                    <CollapseForEmail emailSet={row.original.email} />
                    // console.log(row.original)
                  )
                }
              }
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="Phone" />,
              id: 'phone',
              accessor: 'mobile_no',

              Cell: (row) => {
                if ((row.original.mobile_no).length < 10) {
                  return (<p style={{ color: 'red' }}>mobile no. not added</p>)
                }
                else if (row.original.is_mobile_verified){
                  return (
                  <AfterMobileNoVer setUserMobNo={row.original.mobile_no} />
                  )
              }
                  
                else {
                  return (
                    <CollapseForMob setUserMobNo={row.original.mobile_no} setPlacehold="enter mobile no." setUserId={row.original.user_id}/>
                  )
                }
              },
              minWidth: 300,
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="Profile Photo" />,
              accessor: 'profile_image',
              Cell: (row) => {
                if (row.original.profile_image == null) {
                  return <p style={{ textAlign: 'center', color: 'green' }}>not uploaded</p>
                }
                return <div style={{ textAlign: 'center' }}>
                  <img src={row.original.profile_image} style={{ width: '80px', height: '80px', objectFit: 'cover', border: '1px solid grey' }} alt="profile not found" />
                </div>
              },
              width: 200,
              filterable: false,
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="Cover Photo" />,
              // accessor: 'coverDetails.cover_photo',
              Cell: (row) => {
                if (row.original.cover_photo == null) {
                  return <p style={{ textAlign: 'center', color: 'green' }}>not uploaded</p>
                }
                else {
                  return <div style={{ textAlign: 'center' }}>
                    <img src={row.original.cover_photo} style={{ width: '150px', height: '80px', objectFit: 'cover', border: '1px solid grey' }} alt="cover not found" />
                  </div>
                }
              },
              width: 250,
              filterable: false,
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="Social" />,
              accessor: 'link',
              Cell: (row) => {
                return(
                  <Box display="flex">
                    <FbTwitterInstaVerif checkLink={row.original.cf_facebook_link} checkCondForSocial="facebook" />
                    <FbTwitterInstaVerif checkLink={row.original.cf_twitter_link} checkCondForSocial="twitter"/>
                    <FbTwitterInstaVerif checkLink={row.original.cf_instagram_link} checkCondForSocial="instagram"/>
                  </Box>
                )
              },
              width: 200,
              filterable: false,
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="PAN" />,
              accessor: 'row.original.pan_number',
              Cell: (row) => {
                if (row.original.pan_number === null || row.original.pan_number === 0 || row.original.pan_number === "null") 
                  return (
                    <BeforeAccAdded setUserId={row.original.photographer_id} setPlaceholder="enter Pan No...." setName="pan_no" setNameForVar="pan"/>
                  )
                else if (row.original.pan_verified === null || row.original.pan_verified === 0 || row.original.pan_verified === "null")
                  return (
                    <AccVerification setAcoNo={row.original.pan_number} setUserId={row.original.photographer_id} setNameForVar="pan"/>
                  )
                else {
                  return (
                    <AfterAccInputAndVar setAcoNo={row.original.pan_number}/>
                  )
                }
              },
              width: 300,
              filterMethod: (filter, row) => {
                const id = filter.pivotId || filter.id;
                return (
                  row[id] !== undefined ?
                    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
                );
              },
            },
            {
              Header: (row) => <HeaderWithIcon setHeadName="GST" />,
              accessor: 'row.original.gst_number',
              Cell: (row) => {
                if (row.original.gst_number === null || row.original.gst_number === 0 || row.original.gst_number === "null") 
                  return (
                    <BeforeAccAdded setUserId={row.original.photographer_id} setPlaceholder="enter GST No...." setName="gst_no" setNameForVar="gst"/>
                  )
                else if (row.original.gst_verified === null || row.original.gst_verified === 0 || row.original.gst_verified === "null")
                  return (
                    <AccVerification setAcoNo={row.original.gst_number} setUserId={row.original.photographer_id}  setNameForVar="gst"/>
                  )
                else {
                  return (
                    <AfterAccInputAndVar setAcoNo={row.original.gst_number}/>
                  )
                }
              },
              width: 300,
              filterMethod: (filter, row) => {
                const id = filter.pivotId || filter.id;
                return (
                  row[id] !== undefined ?
                    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
                );
              },
            },
            {
              Header: 'Aadhar',
              accessor: 'row.original.gst_number',
              Cell: (row) => {
                if (row.original.adhar_number === null || row.original.adhar_number === 0 || row.original.adhar_number === "null") 
                  return (
                    <BeforeAccAdded setUserId={row.original.photographer_id} setPlaceholder="enter Aadhar No...." setName="adhar_no" setNameForVar="aadhar"/>
                  )
                else if (row.original.adhar_verified === null || row.original.adhar_verified === 0 || row.original.adhar_verified === "null")
                  return (
                    <AccVerification setAcoNo={row.original.adhar_number} setUserId={row.original.photographer_id} setNameForVar="aadhar"/>
                  )
                else {
                  return (
                    <AfterAccInputAndVar setAcoNo={row.original.adhar_number} setNameForFinal="aadharName"/>
                  )
                }
              },
              width: 300,
              filterMethod: (filter, row) => {
                const id = filter.pivotId || filter.id;
                return (
                  row[id] !== undefined ?
                    String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
                );
              },
            },
          ]}
          defaultPageSize={5}
          className="-striped -highlight venderListTable"
          style={{ height: 'auto' }}
          // defaultFilterMethod={filterCaseInsensitive}
          filterable
          PaginationComponent={Pagination}
        // pages
        />
      </div>
    );
  }
}